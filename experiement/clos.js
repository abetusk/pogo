
var clos_template = {

  // unique identifier of node (supernode as well?)
  //
  "id": -1,

  "descr": "",

  // number of input and output ports
  //
  "inp_n": 0,
  "out_n": 0,

  // network trace connection [<node_id>,<node_port>]
  //
  "trace_inp" : [],
  "trace_out" : [],

  // internal port mapping
  //
  "port_io" : [],
  "port_oi" : [],

  "gfx" : {
  }


};

function clos() {
  this.global_id = 0;
  this.node = {};
  this.snode = {};
  return this;
}

clos.prototype.debug_print = function() {
  console.log("global_id:", this.global_id);
  for (let _nod_id in this.node) {
    let _nod = this.node[_nod_id];
    console.log("--");
    console.log(_nod);
  }
  console.log("===");
}


clos.prototype.create_node = function(n,m,id, descr) {
  id = ((typeof id === "undefined") ? (this.global_id++) : id);
  descr = ((typeof descr == "undefined") ? "" : descr);

  let node = {
    "id": id,
    "descr" : descr,
    "inp_n" : n,
    "out_n" : m,
    "trace_inp" : [],
    "trace_out" : [],
    "port_io" : [],
    "port_oi" : [],
    "gfx" : {
      "bbox": [[0,0],[0,0]],
      "pos" : [0,0],
      "size" : [1,1],
      "anchor": [ [], [] ]
    }
  };

  for (let ii=0; ii<node.inp_n; ii++) {
    node.trace_inp.push([-1,-1]);
    node.port_io.push(-1);
    node.trace_inp.push([-1,-1]);
  }

  for (let ii=0; ii<node.inp_n; ii++) {
    node.trace_out.push([-1,-1]);
    node.port_oi.push(-1);
    node.trace_out.push([-1,-1]);
  }

  //--

  for (let ii=0; ii<node.inp_n; ii++) {
    node.gfx.anchor[0].push( [-0.5, (ii+1)/(node.inp_n+2) ] );
  }

  for (let ii=0; ii<node.out_n; ii++) {
    node.gfx.anchor[1].push( [ 0.5, (ii+1)/(node.inp_n+2) ] );
  }

  this.node[id] = node;

  return node;
}

//--

clos.prototype.gfx_scale = function(id, s) {
  let nod = this.node[id];
  nod.gfx.bbox[0][0] *= s;
  nod.gfx.bbox[0][1] *= s;
  nod.gfx.bbox[1][0] *= s;
  nod.gfx.bbox[1][1] *= s;
  nod.gfx.pos[0] *= s;
  nod.gfx.pos[1] *= s;
  nod.gfx.size[0] *= s;
  nod.gfx.size[1] *= s;
  for (let ii=0; ii<nod.gfx.anchor.length; ii++) {
    nod.gfx.anchor[ii][0] *= s;
    nod.gfx.anchor[ii][1] *= s;
  }
}

clos.prototype.gfx_pos = function(id, p) {
  let nod = this.node[id];
  nod.gfx.pos[0] = p[0];
  nod.gfx.pos[1] = p[1];
}

clos.prototype.gfx_dot_out = function() {
  let count = 0;
  let l = [];

  console.log("digraph G {");
  console.log("  node [shape=record];");
  for (let _nod_id in this.node) {
    let _nod = this.node[_nod_id];

    if (((count%3)==0) && (l.length > 0)) {
      //console.log("  { rank=same", l.join(" "), " }");
      l = [];
    }

    let dotname = "n" + _nod.id;


    l.push(dotname);

    console.log("  n" + _nod.id + " [pos=\"" + _nod.gfx.pos[0].toString() + "," + _nod.gfx.pos[1].toString() + "!\"]");

    for (let ii=0; ii<_nod.trace_inp.length; ii++) {
      if (_nod.trace_out[ii][0] >= 0) {
        //console.log("  n" + _nod.id + " -> n" + _nod.trace_out[ii][0] + ";");
        console.log("  n" + _nod.trace_out[ii][0] + " -> n" + _nod.id + ";");
      }
      if (_nod.trace_inp[ii][0] >= 0) {
        console.log("  n" + _nod.trace_inp[ii][0] + " -> n" + _nod.id + ";");
      }
    }

    count++;
  }
  if (l.length > 0) {
    //console.log("  { rank=same", l.join(" "), " }");
    l = [];
  }

  console.log("}");
}

//---

function _realize_port(opt) {
  opt = ((typeof opt === "undefined") ? {} : opt);

  let x = ((typeof opt['x'] === "undefined") ? 200 : opt['x']);
  let y = ((typeof opt['y'] === "undefined") ? 100 : opt['y']);

  let w = ((typeof opt['w'] === "undefined") ? 50 : opt['w']);
  let h = ((typeof opt['h'] === "undefined") ? 100 : opt['h']);

  let s = ((typeof opt['s'] === "undefined") ? 10 : opt['s']);

  let n = ((typeof opt['n'] === "undefined") ? 3 : opt['n']);
  let m = ((typeof opt['m'] === "undefined") ? 3 : opt['m']);

  opt.port = [ [], [] ];

  for (let ii=0; ii<n; ii++) {
    let ly =  h/(2) -  h*((ii+1)/(n+1))
    opt.port[0].push([-w/2-s, ly]);
  }

  for (let ii=0; ii<m; ii++) {
    let ly =  h/(2) -  h*((ii+1)/(m+1))
    opt.port[1].push([w/2+s, ly]);
  }

  return opt;
}

function _two_draw_switch(two, opt) {
  opt = ((typeof opt === "undefined") ? {} : opt);

  let x = ((typeof opt['x'] === "undefined") ? 200 : opt['x']);
  let y = ((typeof opt['y'] === "undefined") ? 100 : opt['y']);

  let w = ((typeof opt['w'] === "undefined") ? 50 : opt['w']);
  let h = ((typeof opt['h'] === "undefined") ? 100 : opt['h']);

  let s = ((typeof opt['s'] === "undefined") ? 10 : opt['s']);

  let n = ((typeof opt['n'] === "undefined") ? 3 : opt['n']);
  let m = ((typeof opt['m'] === "undefined") ? 3 : opt['m']);

  var rect = two.makeRectangle(x, y, w, h);

  var rgb = ((typeof opt["fill"] === "undefined") ? _rnd_rgb() : opt["fill"]);

  //rect.fill = 'rgb(0, 0, 0)';
  //rect.fill = _rnd_rgb();
  rect.fill = rgb;
  rect.opacity = 0.75;
  //rect.noStroke();

  for (let ii=0; ii<n; ii++) {
    let _l = two.makeLine(
        x + opt.port[0][ii][0] , y + opt.port[0][ii][1],
        x + opt.port[0][ii][0] + s, y + opt.port[0][ii][1] );
  }

  for (let ii=0; ii<m; ii++) {
    let _l = two.makeLine(
        x + opt.port[1][ii][0], y + opt.port[1][ii][1],
        x + opt.port[1][ii][0] - s, y + opt.port[1][ii][1] );
  }

  two.update();
}

function _rnd_rgb() {
  let rgb = [
    Math.floor(Math.random()*255),
    Math.floor(Math.random()*255),
    Math.floor(Math.random()*255) ];
  return "rgb(" + rgb[0].toString() + "," + rgb[1].toString() + "," + rgb[2].toString() + ")";

}

clos.prototype.gfx_two = function() {
  var elem = document.getElementById('draw-shapes');
  //var params = { width: 285, height: 200 };
  var params = { width: 400, height: 400 };
  var two = new Two(params).appendTo(elem);

  let count = 0;
  let l = [];

  let switch_info = {};

  for (let _nod_id in this.node) {
    let _nod = this.node[_nod_id];
    let _opt = {
      "n": _nod.inp_n,
      "m": _nod.out_n,
      "x": _nod.gfx.pos[0] + 200,
      "y": _nod.gfx.pos[1] + 200,
      "s": 10,
      "w": 25,
      "h": 50,
      "fill":_rnd_rgb()
    };
    _realize_port(_opt);
    _two_draw_switch(two, _opt);

    switch_info[_nod_id] = _opt;
  }

  for (let _nod_id in this.node) {
    let _nod = this.node[_nod_id];

    for (let src_port=0; src_port<_nod.trace_inp.length; src_port++) {
      let dst_id = _nod.trace_inp[src_port][0];
      if (dst_id < 0) { continue; }

      let dst_port = _nod.trace_inp[src_port][1];

      let src_id = _nod.id;

      let x0 = switch_info[src_id].port[0][src_port][0];
      let y0 = switch_info[src_id].port[0][src_port][1];

      x0 += switch_info[src_id].x;
      y0 += switch_info[src_id].y;

      let x1 = switch_info[dst_id].port[1][dst_port][0];
      let y1 = switch_info[dst_id].port[1][dst_port][1];

      x1 += switch_info[dst_id].x;
      y1 += switch_info[dst_id].y;

      //console.log(x0,y0, x1,y1);

      let _l = two.makeLine(x0,y0, x1,y1);
      //_l.stroke = "rgb(128,0,0)";
      _l.stroke = _rnd_rgb();


    }

    //console.log(_nod.trace_inp, _nod.trace_out);
  }

  two.update();
}


//--


clos.prototype.wire_trace_oi = function(x_id_port, y_id_port) {
  let x = this.node[x_id_port[0]];
  let y = this.node[y_id_port[0]];

  x.trace_out[x_id_port[1]][0] = y_id_port[0];
  x.trace_out[x_id_port[1]][1] = y_id_port[1];

  y.trace_inp[y_id_port[1]][0] = x_id_port[0];
  y.trace_inp[y_id_port[1]][1] = x_id_port[1];
}

clos.prototype.switch_map_io = function(id, port_inp, port_out) {
  let x = this.node[id];
  x.port_io[port_inp] = port_out;
  x.port_oi[port_out] = port_inp;
}

function clos_node_2_3() {
  let nod = {
    "id": -1,
    "inp_n" : 2,
    "out_n" : 3,
    "trace_inp" : [[-1,-1],[-1,-1]],
    "trace_out" : [[-1,-1],[-1,-1],[-1,-1]],
    "port_io" : [0,1],
    "port_oi" : [0,1,-1]
  };
  return nod;
}

function clos_node_3_2() {
  let nod = {
    "id": -1,
    "inp_n" : 3,
    "out_n" : 2,
    "trace_inp" : [[-1,-1],[-1,-1],[-1,-1]],
    "trace_out" : [[-1,-1],[-1,-1]],
    "port_io" : [0,1,-1],
    "port_oi" : [0,1]
  };
  return nod;
}

function clos_node_3_3() {
  let nod = {
    "id": -1,
    "inp_n" : 3,
    "out_n" : 3,
    "trace_inp" : [[-1,-1],[-1,-1],[-1,-1]],
    "trace_out" : [[-1,-1],[-1,-1],[-1,-1]],
    "port_io" : [0,1,2],
    "port_oi" : [0,1,2]
  };
  return nod;
}

function clos_node(n,m,id)  {
}


function clos_node_6_6() {

  ctx = clos_ctx();

  let inp = [ clos_node_2_3(), clos_node_2_3(), clos_node_2_3() ];
  let out = [ clos_node_3_2(), clos_node_3_2(), clos_node_3_2() ];
  let mid = [ clos_node_3_3(), clos_node_3_3(), clos_node_3_3() ];

  for (let ii=0; ii<3; ii++) {
    inp.id = ctx.global_id;
    ctx.global_id++;

    out.id = ctx.global_id;
    ctx.global_id++;

    mid.id = ctx.global_id;
    ctx.global_id++;
  }

  for (let ii=0; ii<3; ii++) {
    inp.trace_out[ii][0] = mid[ii].id;
    inp.trace_out[ii][1] = ii;

    out.trace_inp[ii][0] = mid[ii].id;
    out.trace_inp[ii][1] = ii;

    mid.trace_inp[ii]
  }
}

//---

function _snode(clo) {
}

function _clo_r_l1(clo) {
  let inp = [], out = [], mid = [];
  for (let ii=0; ii<3; ii++) {
    inp.push( clo.create_node(2,3, undefined, "inp" + ii.toString()) );
    mid.push( clo.create_node(3,3, undefined, "mid" + ii.toString()) );
    out.push( clo.create_node(3,2, undefined, "out" + ii.toString()) );
  }

  for (let ii=0; ii<3; ii++) {
    for (let jj=0; jj<3; jj++) {
      clo.wire_trace_oi( [inp[ii].id, jj], [mid[jj].id, ii] );
      clo.wire_trace_oi( [mid[ii].id, jj], [out[jj].id, ii] );
    }
  }

  //clos.snode

  //--

  let _F = 100;

  let _N = 3;
  for (let ii=0; ii<_N; ii++) {
    clo.gfx_pos( inp[ii].id, [-_F, ((_N-1)/2 - ii)*_F ] );
    clo.gfx_pos( mid[ii].id, [ 0, ((_N-1)/2 - ii)*_F ] );
    clo.gfx_pos( out[ii].id, [ _F, ((_N-1)/2 - ii)*_F ] );
  }

}

var g_clo = {};

function _main() {
  let clo = new clos();
  _clo_r_l1(clo);
  //clo.gfx_dot_out();
  clo.gfx_two();

  g_clo = clo;
}

function _main_old() {

  let _n0 = clo.create_node(2,3);
  let _n1 = clo.create_node(2,3);


  clo.wire_trace_oi([_n0.id, 0], [_n1.id,0]);
  clo.switch_map_io(0, 0, 1);

  clo.debug_print();

  //console.log(clo);
  //console.log(clo.global_id);
}

_main();
